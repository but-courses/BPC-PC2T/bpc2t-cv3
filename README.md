# Course: BPC2T - seminar 3
This project provides a solution for seminar 3. Includes topics like:
1. Class creation 
2. Java access modifiers
3. Class inheritance
4. Abstract classes
5. Interfaces
6. @Override annotation and method overriding
7. Object casting and instanceof operator

## Authors

email | Name 
------------ | -------------
pavelseda@email.cz | Šeda Pavel

## Starting up Project
Just import it in your selected IDE (Eclipse, IntelliJ IDEA, Netbeans, ...)

## Used Technologies
The project was built and tested with these technologies, so if you have any unexpected troubles let us know.

```
Maven         : 3.3.9
Java          : OpenJDK 11
```
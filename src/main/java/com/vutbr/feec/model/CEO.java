package com.vutbr.feec.model;

/**
 * 
 * @author Pavel Seda
 *
 */
public class CEO extends AbstractEmployee {

	public CEO(String email, String name, int age, long salary, boolean gender) {
		super(email, name, age, salary, gender);
	}

	@Override
	public void work() {
		System.out.println("Pracovn�k: " + getEmail() + " pracuje na pozici: " + this.getClass().getName());
	}

}

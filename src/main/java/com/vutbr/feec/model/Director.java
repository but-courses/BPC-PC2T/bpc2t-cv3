package com.vutbr.feec.model;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Director extends AbstractEmployee {

	public Director(String email, String name, int age, long salary, boolean gender) {
		super(email, name, age, salary, gender);
	}

	@Override
	public void work() {
		System.out.println("Pracovník: " + getEmail() + " pracuje na pozici: " + this.getClass().getName());
	}

	/**
	 * Metody s klíčovým slovem final není možné přepsat v potomkovi (klidně
	 * vyzkoušejte)
	 */
	public final void removeCompany() {
		System.out.println("It is not possible to remove company.");
	}

}

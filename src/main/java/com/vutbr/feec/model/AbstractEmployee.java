package com.vutbr.feec.model;

import com.vutbr.feec.iface.CompanyRules;

/**
 * 
 * @author Pavel Seda
 *
 */
public abstract class AbstractEmployee implements CompanyRules {

	protected String email;
	protected String name;
	protected int age;
	protected long salary;
	protected boolean gender;

	public AbstractEmployee(String email, String name, int age, long salary, boolean gender) {
		super();
		setEmail(email);
		setName(name);
		setAge(age);
		setSalary(salary);
		setGender(gender);
	}

	public abstract void work();

	public void sayHello() {
		System.out.println("Hello.");
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public long getSalary() {
		return salary;
	}

	public void setSalary(long salary) {
		this.salary = salary;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	@Override
	public String toString() {
		return "Employee [email=" + email + ", name=" + name + ", age=" + age + ", salary=" + salary + ", gender="
				+ gender + "]";
	}

}

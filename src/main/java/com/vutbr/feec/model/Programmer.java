package com.vutbr.feec.model;

/**
 *
 * @author Pavel Seda
 *
 */
public class Programmer extends AbstractEmployee {

	public Programmer(String email, String name, int age, long salary, boolean gender) {
		super(email, name, age, salary, gender);
	}

	@Override
	public void work() {
		System.out.println("Pracovn�k: " + getEmail() + " pracuje na pozici: " + this.getClass().getName());
	}

}

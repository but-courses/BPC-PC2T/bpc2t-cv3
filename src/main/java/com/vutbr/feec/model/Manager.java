package com.vutbr.feec.model;

/**
 * @author Pavel Seda
 */
public class Manager extends AbstractEmployee {

    public Manager(String email, String name, int age, long salary, boolean gender) {
        super(email, name, age, salary, gender);
    }

    @Override
    public void work() {
        System.out.println("Pracovník: " + getEmail() + " pracuje na pozici: " + this.getClass().getName());
    }

    public void raiseSalary(long salary) {
        if ((getSalary() + salary) < 0) {
            System.out.println("Není možné snížit plat do záporných čísel");
        }
        setSalary(getSalary() + salary);
    }

}

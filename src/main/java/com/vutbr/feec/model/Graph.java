package com.vutbr.feec.model;

import java.util.List;

/**
 * 
 * Tato třída reprezentuje Graf, kde G=(V,E), V(vertex) jsme zde nazvali Node.
 * 
 * V praxi bychom tuto třídu implementovali genericky (pozdější látka).
 * 
 * @author Pavel Seda
 *
 */
public class Graph {

	private List<Node> nodes;

	/**
	 * Vnořená třída Node.
	 * 
	 * @author Pavel Seda
	 *
	 */
	public class Node {
		public String name;
		public List<Edge> connections;
	}

	/**
	 * Vnořená třída Edge.
	 * 
	 * @author Pavel Seda
	 *
	 */
	public class Edge {
		public Node start;
		public Node end;
		public double weight;
	}

	public Graph(List<Node> nodes) {
		super();
		this.nodes = nodes;
	}

	public List<Node> getNodes() {
		return nodes;
	}

	public void setNodes(List<Node> nodes) {
		this.nodes = nodes;
	}

}

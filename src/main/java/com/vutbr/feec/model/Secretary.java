package com.vutbr.feec.model;

/**
 * 
 * @author Pavel Seda
 *
 */
public class Secretary extends AbstractEmployee {

	public Secretary(String email, String name, int age, long salary, boolean gender) {
		super(email, name, age, salary, gender);
	}

	@Override
	public void work() {
		System.out.println("Pracovník: " + getEmail() + " pracuje na pozici: " + this.getClass().getName());
	}

	public void sendEmail(String from, String to, String content) {
		if (content == null || content.isEmpty()) {
			System.out.println("Zapomněli jste napsat tělo emailu..");
		}
		System.out.println("Email sent by secretary: " + System.lineSeparator() + "from: " + from
				+ System.lineSeparator() + "to: " + to + System.lineSeparator() + "content: " + content);
	}

}

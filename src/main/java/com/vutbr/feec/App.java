package com.vutbr.feec;

import com.vutbr.feec.model.CEO;
import com.vutbr.feec.model.Director;
import com.vutbr.feec.model.AbstractEmployee;
import com.vutbr.feec.model.Manager;
import com.vutbr.feec.model.Programmer;
import com.vutbr.feec.model.Secretary;

/**
 * 
 * @author Pavel Seda
 *
 */
public class App {

	public static void main(String[] args) {
		AbstractEmployee director = new Director("pavelseda@email.cz", "Pavel Seda", 12, 10000L, true);
		AbstractEmployee secretary = new Secretary("milena@email.cz", "Milena Novakova", 13, 2000L, false);
		AbstractEmployee manager = new Manager("jindrich@email.cz", "Jindrich Zatopek", 14, 3000L, true);
		AbstractEmployee ceo = new CEO("pepa@email.cz", "Josef Novak", 15, 1000L, true);
		AbstractEmployee programmer = new Programmer("karel@email.cz", "Karel Novak", 16, 2000L, true);

		// tomuto jevu se říká Polymorfismus
		// (vykonává se metoda na základě instance objektu (nikoliv typu objektu)
		// všechny metody v javě jsou polymorfní
		// (v C++ či C# by bylo nutné deklarovat tyto metody klíčovým slovem virtual
		director.work();
		secretary.work();
		manager.work();
		ceo.work();
		programmer.work();

		// přetypování objektu a využití klíčového slova instanceof
		if (secretary instanceof Secretary) {
			Secretary sec = (Secretary) secretary;
			sec.sendEmail("pavelseda@email.cz", "pavelseda@email.cz", "Je fajn, ze mame reseni prikladu.");
		}

		// přetypování objektu a využití klíčového slova instanceof
		if (manager instanceof Manager) {
			((Manager) manager).raiseSalary(1000L);
		}

	}

}
